module.exports = async (req, res) => {
	const conn = await req.conn
	const body = req.body
	const users_id = req.decodedToken.USERS_ID

	const insertSQL = `
    INSERT INTO
	mp_emp_new 
    (
	request_job_id,
	total_amount,
	emp_new_status,
	createby,
	createdate,
	updateby,
	updatedate

    )values(

    to_number(${body.request_job_id}),
	to_number(${body.newEmpBank.length}),
    1,
    to_number(${users_id}),
    sysdate,
    NULL,
    NULL)
    `
	console.log("insertSQL", insertSQL)

	const insertResult = await conn.execute(insertSQL)

	await conn.commit()

	res.resly(200, {})
}
