module.exports = async (req, res) => {
	const conn = await req.conn
	const body = req.body
	const users_id = req.decodedToken.USERS_ID

	const updateSQL = `
    UPDATE 
        mp_emp_new
    SET 
        total_amount = to_number(${body.newEmpBank.length})
	WHERE
		request_job_id = to_number(${body.request_job_id})
    `
	console.log("updateSQL", updateSQL)

	const updateResult = await conn.execute(updateSQL)

	await conn.commit()

	res.resly(200, {})
}
