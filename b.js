const services = require("../../services")
const lib = require("../../lib")
const bcrypt = require("bcrypt")
const Errorly = require("../../lib/response/Errorly")

module.exports = async (req, res) => {
	const conn = await req.conn
	const body = req.body
	console.log(body)
	const users_id = req.decodedToken.USERS_ID
	const sql = `

    UPDATE mp_request_job
    SET 
        job_position_id,
        plantno,
        division_id,
        department_id,
        section_id,
        tel,
        request_amount,
        request_date,
        request_type,
        remark,
        education_type_id,
        education_other,
        gender,
        age,
        shiftwork,
        work_exp,
        computer_skill,
        talent,
        position_no,
        request_job_status,
        users_id,
        close_flag,
        close_date,
        close_remark,
        createby,
        createdate,
        updateby,
        updatedate,
        close_by,
        man_act,
        dept_quota,
        excelflag,
        pointflag,
        flow_status
    WHERE request_job_id = '${request_job_id}'

    INSERT INTO  (
        docno,
        docdate,
        
        
    ) VALUES (
        (TO_CHAR(SYSDATE,'YYMM')  || '0000'  + MP_REQUEST_JOB_DOCNO_SEQ.nextval),
        sysdate,
        to_number(${body.job_position_id}),
        to_number(${body.plantno}),
        to_number(${body.division_id}),
        to_number(${body.department_id}),
        to_number(${body.section_id}),
        '${body.tel}',
        to_number(${body.request_amount}),
        TO_DATE('${body.request_date}', 'dd/mm/yyyy'),
        to_number(${body.request_type}),
        '${body.remark}',
        to_number(${body.education_type_id}),
        NULL,
        to_number(${body.gender}),
        to_number(${body.age}),
        to_number(${body.shiftwork}),
        to_number(${body.work_exp}),
        '${body.computer_skill}',
        '${body.talent}',
        NULL,
        1,
        to_number(${users_id}),
        0,
        NULL,
        NULL,
        to_number(${users_id}),
        sysdate,
        NULL,
        NULL,
        NULL,
        to_number(${body.man_act}),
        to_number(${body.dept_quota}),
        to_number(${body.excelflag}),
        to_number(${body.pointflag}),
        'NEW'
    
    )

     `

	console.log(sql)
	const bindParams = {}

	const result = await conn.execute(sql, bindParams)

	let sql2 = `
    UPDATE mp_request_job
    SET position_no = to_number(( SELECT (DOCNO || request_amount) FROM MP_REQUEST_JOB mrj WHERE mrj.rowid = '${result.lastRowid}'))
    WHERE rowid='${result.lastRowid}'
    `

	const bindParams2 = {}

	const result2 = await conn.execute(sql2, bindParams2)

	if (req.body.job_replace.length > 0) {
		const replaceJobSql = `
    INSERT INTO
	mp_request_replace (
	request_job_id,
	resign_type,
	resign_date,
	emp_id,
	emp_position,
	emp_name,
	replace_status,
	createby,
	createdate,
	updateby,
	updatedate
)values(
    to_number(( SELECT request_job_id FROM MP_REQUEST_JOB mrj WHERE mrj.rowid = '${result.lastRowid}')),
    :resign_type,
    TO_DATE(:resign_date, 'dd/mm/yyyy'),
	:emp_id,
	:emp_position,
	:emp_name,
    1,
    to_number(${users_id}),
    sysdate,
    NULL,
    NULL)
    
    `

		const bindParams3 = req.body.job_replace

		console.log(replaceJobSql)
		console.log("bindParams3", bindParams3)

		const replaceJObResult = await conn.executeMany(replaceJobSql, bindParams3)
	}

	await conn.commit()

	res.resly(200, {})
}
