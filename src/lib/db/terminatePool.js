const oracledb = require("oracledb")
let terminatedStatus = false

module.exports = () => {
  return new Promise(async (resolve, reject) => {
    let pool = await oracledb.getPool()
    try {
      if (pool && !terminatedStatus && pool.status === 6000) {
        terminatedStatus = true
        process.nextTick(async () => {
          await pool.close(0)
          console.log("terminate pool success")
          resolve()
          return
        })
      } else {
        console.error("No Pool")
        resolve()
      }
    } catch (error) {
      console.error("Pool Error", error)
      resolve()
    }
  })
}
