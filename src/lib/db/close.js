module.exports = async (conn) => {
  if (conn) {
    try {
      await conn.close()
      // console.error("=== Close Conn Success", conn.unix)
    } catch (error) {
      // console.error("=== Close Conn Fail", conn.unix, error)
    }
  }
}
