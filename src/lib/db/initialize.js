const db = require(".")
const processMonitor = require("./processMonitor")
module.exports = async () => {
  db.conn()
    .then((conn) => {
      console.log("DB Conn Initialize Success")
      conn.close()
    })
    .catch((e) => {
      console.error("DB Conn Initialize Error", e)
    })
  processMonitor()
}
