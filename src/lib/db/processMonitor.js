const terminatePool = require("./terminatePool")
const exitHook = require("async-exit-hook")
module.exports = () => {
    console.log("DB Monitoring")
    exitHook(async (callback) => {
        console.log("Exiting DB Pool")
        await terminatePool()
        callback()
    })
}
