const oracledb = require("oracledb")
const db = require(".")

module.exports = async (req, res, next) => {
  req.conn = await db.conn()

  next()
}
