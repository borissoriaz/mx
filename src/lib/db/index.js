module.exports = {
  get initialize() {
    return require("./initialize")
  },
  get mw() {
    return require("./mw")
  },
  get conn() {
    return require("./conn")
  },
  get close() {
    return require("./close")
  },
}
