const oracledb = require("oracledb")
const path = require("path")

oracledb.outFormat = oracledb.OUT_FORMAT_OBJECT
// const instantPath = path.resolve("./src/lib/db/instantclient")
// oracledb.initOracleClient({ libDir: instantPath })
oracledb.initOracleClient({ libDir: "C:\\oracle\\instantclient_21_3" })
const config = {
	user: "hrms01",
	password: "01hrms",
	connectString: "192.1.1.240:1521/hrms",
}
module.exports = async () => {
	let conn
	try {
		if (process.env.isPool) {
			if (!global.pool) {
				console.log("==> create pool Pool")

				global.pool = true
				await oracledb.createPool(config)
			}
			console.log("==> create conn Pool")
			const pool = await oracledb.getPool()
			conn = pool.getConnection()
		} else {
			console.log("==> create conn STAL")
			conn = oracledb.getConnection(config)
		}
		return conn
	} catch (error) {
		console.error("error", error)
	}
}
