const Errorly = require("../../lib/response/Errorly")
module.exports = (req) => {
	if (!req.headers["authorization"]) {
		throw new Errorly(401, "No authorization token provided.")
	}

	const accessToken = req.headers["authorization"].split(" ")[1]
	return accessToken
}
