module.exports = {
  get decodeToken() {
    return require("./decodeToken.util")
  },
  get signToken() {
    return require("./signToken.util")
  },
  get verifyToken() {
    return require("./verifyToken.util")
  },
  get getAccessToken() {
    return require("./getAccessToken.util")
  },
  get mw() {
    return require("./mw")
  },
}
