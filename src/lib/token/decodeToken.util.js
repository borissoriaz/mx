const jsonwebtoken = require("jsonwebtoken")
module.exports = (token) => {
  const decodedToken = jsonwebtoken.decode(token)

  return decodedToken
}
