const jsonwebtoken = require("jsonwebtoken")
module.exports = (payload) => {
  const signedToken = jsonwebtoken.sign(payload, process.env.ACCESS_TOKEN_SECRET_KEY, {
    expiresIn: parseInt(process.env.ACCESS_TOKEN_EXPIRATION_TIME),
  })

  return signedToken
}
