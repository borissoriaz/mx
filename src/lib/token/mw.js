const { getAccessToken, decodeToken, verifyToken } = require(".")
const Errorly = require("../../lib/response/Errorly")

module.exports = (req, res, next) => {
  const accessToken = getAccessToken(req)
  req.accessToken = accessToken
  try {
    const decodedToken = verifyToken(accessToken)
    req.decodedToken = decodedToken
  } catch (error) {
    throw new Errorly(401, "Unauthorized", error)
  }
  next()
}
