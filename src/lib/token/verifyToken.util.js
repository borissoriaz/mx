const jsonwebtoken = require("jsonwebtoken")
module.exports = (token) => {
  const decoded = jsonwebtoken.verify(token, process.env.ACCESS_TOKEN_SECRET_KEY)
  return decoded
}
