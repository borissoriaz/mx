module.exports = {
  get mw() {
    return require("./mw")
  },
  get errorHandler() {
    return require("./errorHandler")
  },
}
