module.exports = function (req, res) {
	return (
		status = 500,
		data = [],
		customResponse = "Some Thing Went Wrong",
		error,
		stack,
		extra
	) => {
		console.error({ error, stack })
		res.status(status)
		res.json({
			rqid: req.rqid,
			code: status,
			success: false,
			message: customResponse,
			data,
			debug: { error, stack },
			...extra,
		})
	}
}
