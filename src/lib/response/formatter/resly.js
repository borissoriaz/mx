module.exports = function (req, res) {
	return (status = 200, { data = [], message = "Success", ...extra }) => {
		res.status(status)
		res.json({
			rqid: req.rqid,
			code: status,
			success: true,
			message: message,
			data,
			extra,
		})
	}
}
