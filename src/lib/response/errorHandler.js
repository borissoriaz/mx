module.exports = (app) => {
  app.use((err, req, res, next) => {
    res.catchly(err?.status, [], err?.message, err?.error, err?.stack)
    next()
  })
}
