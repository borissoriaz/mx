class Errorly extends Error {
    constructor(status, message, error) {
        super(message)

        this.status = status || 500
        this.error = error || []
    }
}

module.exports = Errorly
