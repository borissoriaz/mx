const resly = require("./formatter/resly")
const catchly = require("./formatter/catchly")

module.exports = (req, res, next) => {
  res.resly = resly(req, res)
  res.catchly = catchly(req, res)

  next()
}
