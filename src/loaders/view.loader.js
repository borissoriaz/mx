const path = require("path")
const express = require("express")
module.exports = (app) => {
	app.use(express.static(path.join(__dirname, "dist")))

	app.get("/*", function (req, res) {
		try {
			res.sendFile(path.join(__dirname, "dist") + "/" + "index.html")
		} catch (error) {
			console.log("error", error)
		}
	})
}
