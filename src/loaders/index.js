const expressLoader = require("./express.loader")
const db = require("../lib/db")
const routes = require("../routes")
const response = require("../lib/response")
const statusMount = require("../utils/system/status.util")
const primitiveLoader = require("./primitive.loader")
const viewLoader = require("./view.loader")

module.exports = async (app) => {
	expressLoader(app)
	primitiveLoader(app)
	await db.initialize()
	app.use(db.mw)
	app.use(response.mw)
	routes(app)
	statusMount(app)
	viewLoader(app)
	response.errorHandler(app)
}
