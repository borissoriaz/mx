const crypto = require("crypto")

module.exports = (app) => {
	app.use(rqidGeneratorMW)

	// app.post("/api/upload", upload.single("file"), function (req, res) {
	// 	if (!req.file) {
	// 		console.error("FILE_MISSING", req)
	// 	} else {
	// 		res.send({ status: "success" })
	// 	}
	// })

	app.use("/favicon.ico", (req, res) => res.status(204).end())
}

const rqidGeneratorMW = (req, res, next) => {
	req.rqid = crypto.randomUUID({ disableEntropyCache: true })
	next()
}
