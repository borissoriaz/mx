const controllers = require("../controllers")
const handler = require("../utils/handler/handlerAsync.util")

module.exports = (router) => {
  router.route("/auth/login").post(handler(controllers.auth.login))

  router.route("/auth/signup").post(handler(controllers.auth.signup))

  router.route("/auth/logout").post(handler(controllers.auth.login))

  return router
}
