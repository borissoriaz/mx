const controllers = require("../controllers")
const handler = require("../utils/handler/handlerAsync.util")
const lib = require("../lib")

module.exports = (router) => {
	router.use(lib.token.mw)

	router.route("/approval").get(handler(controllers.approval.getApprovalFlow))
	// .post(handler(controllers.form.saveForm))

	router
		.route("/approval/detail")
		.get(handler(controllers.approval.getApprovalFlowDetail))

	router.route("/approval/summit").post(handler(controllers.approval.summitApproval))

	router.route("/approval/hold").post(handler(controllers.approval.holdApproval))

	router.route("/approval/reject").post(handler(controllers.approval.rejectApproval))

	router.route("/approval/do").post(handler(controllers.approval.doApproval))

	router.route("/approval/list/hr").get(handler(controllers.approval.getHrFlowList))

	router.route("/approval/list").get(handler(controllers.approval.getApprovalFlowList))

	router.route("/approval/retry").post(handler(controllers.approval.reApproval))

	return router
}
