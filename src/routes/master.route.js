const controllers = require("../controllers")
const handler = require("../utils/handler/handlerAsync.util")
const lib = require("../lib")

module.exports = (router) => {
	router.use(lib.token.mw)
	router.route("/master/department").post(handler(controllers.master.department))
	router.route("/master/job-type").post(handler(controllers.master.jobType))
	router.route("/master/position").post(handler(controllers.master.position))
	router.route("/master/label").post(handler(controllers.master.label))
	router.route("/master/all").post(handler(controllers.master.all))
	router.route("/master/plant/add").post(handler(controllers.master.plant.add))
	router.route("/master/plant/update").post(handler(controllers.master.plant.update))
	router.route("/master/plant/delete").post(handler(controllers.master.plant.delete))
	router.route("/master/division/add").post(handler(controllers.master.division.add))
	router
		.route("/master/division/update")
		.post(handler(controllers.master.division.update))
	router
		.route("/master/division/delete")
		.post(handler(controllers.master.division.delete))
	router.route("/master/department/add").post(handler(controllers.master.department.add))
	router
		.route("/master/department/update")
		.post(handler(controllers.master.department.update))
	router
		.route("/master/department/delete")
		.post(handler(controllers.master.department.delete))
	router.route("/master/section/add").post(handler(controllers.master.section.add))
	router.route("/master/section/update").post(handler(controllers.master.section.update))
	router.route("/master/section/delete").post(handler(controllers.master.section.delete))

	return router
}
