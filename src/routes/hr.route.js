const controllers = require("../controllers")
const handler = require("../utils/handler/handlerAsync.util")
const lib = require("../lib")

module.exports = (router) => {
	router.use(lib.token.mw)
	// router
	// 	.route("/form")
	// 	.get(handler(controllers.form.getForm))
	// 	.post(handler(controllers.form.saveForm))

	router.route("/hr/detail").get(handler(controllers.hr.getFormDetailHr))
	router.route("/hr/savenewemp").post(handler(controllers.hr.saveNewEMP))
	router.route("/hr/updatenewemp").post(handler(controllers.hr.editNewEMP))

	// .patch(handler(controllers.form.updateForm))

	// router.route("/form/close").post(handler(controllers.form.closeForm))

	return router
}
