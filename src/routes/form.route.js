const controllers = require("../controllers")
const handler = require("../utils/handler/handlerAsync.util")
const lib = require("../lib")

module.exports = (router) => {
	router.use(lib.token.mw)
	router
		.route("/form")
		.get(handler(controllers.form.getForm))
		.post(handler(controllers.form.saveForm))

	router.route("/form/update").post(handler(controllers.form.updateForm))

	router.route("/form/detail").get(handler(controllers.form.getFormDetail))

	router.route("/form/close").post(handler(controllers.form.closeForm))

	return router
}
