const { validate } = require("express-validation")
const express = require("express")
const fileupload = require("express-fileupload")

const lib = require("../lib")

const router = express.Router()
const authRoute = require("./auth.route")
const masterRoute = require("./master.route")
const blazeRoute = require("./blaze.route")
const formRoute = require("./form.route")
const approvalRoute = require("./approval.route")
const hrRoute = require("./hr.route")
const uuid = require("uuid")
const Errorly = require("../lib/response/Errorly")
// const upload = require("./upload")

// const Validator = require("../validators")

module.exports = (app) => {
	// Application routes --------------

	app.use(
		fileupload({
			createParentPath: true,
			limits: { fileSize: 50 * 1024 * 1024 },
			useTempFiles: true,
		})
	)

	app.post("/api/upload", async (req, res) => {
		if (!req.files) {
			throw new Errorly("No file uploaded")
		} else {
			let file = req.files.file

			const fileName = uuid.v4() + "-" + file.name
			const storagePath = "./src/loaders/dist/files/"

			file.mv(storagePath + fileName)

			res.resly(200, { data: { fileName } })
		}
	})

	app.use("/api", router)
	authRoute(router)
	masterRoute(router)
	blazeRoute(router)
	formRoute(router)
	approvalRoute(router)
	hrRoute(router)
	// app.post("/api/upload", upload.single("file"), function (req, res) {
	// 	console.log("incoming file")
	// 	if (!req.file) {
	// 	} else {
	// 		res.send({ status: "success", filename: req.filename })
	// 	}
	// })

	// End Application routes -----------
}
