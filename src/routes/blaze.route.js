const controllers = require("../controllers")
const handler = require("../utils/handler/handlerAsync.util")
const lib = require("../lib")

module.exports = (router) => {
	// router.use(lib.token.mw)

	// router.route("/blaze").post(handler(controllers.blaze.signal))
	router.route("/ora").get(handler(controllers.blaze.ora))

	return router
}
