const express = require("express")
const http = require("http")
const loaders = require("./loaders")
require("dotenv").config()

function main() {
	const app = express()
	const server = http.createServer(app)
	const port = process.env.PORT || 30301
	server.listen(port, (error) => {
		if (error) {
			console.error("server failed to start", error)
			process.exit(1)
		}
		console.info(`server started [PORT] = [${port}]`)
	})

	loaders(app)
}

main()
