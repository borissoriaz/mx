const db = require("../../lib/db")
module.exports = (func) => (req, res, next) =>
	func(req, res)
		.catch((error) => {
			req.conn.rollback()
			next(error)
		})
		.finally(() => db.close(req.conn))
