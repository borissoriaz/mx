module.exports = () => {
    process
        .on("unhandledRejection", (reason, promise) => {
            console.log("Unhandled Rejection at:", promise, "reason:", reason)
        })
        .on("warning", (warning) => {
            console.log("warning")
            console.warn(warning.name)
            console.warn(warning.message)
            console.warn(warning.stack)
        })
}
