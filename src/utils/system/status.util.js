const handler = require("../handler/handlerAsync.util")

module.exports = (app) => {
  app.use(
    "/status",
    handler((req, res) => {
      res.json({ message: "ok" })
    })
  )
}
