module.exports = {
	get auth() {
		return require("./auth")
	},
	get approval() {
		return require("./approval")
	},
}
