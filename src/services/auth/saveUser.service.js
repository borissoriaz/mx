module.exports = async (
	conn,
	username = null,
	loginname = null,
	password = null,
	email = null,
	tel = null
) => {
	const sql = ` INSERT INTO HRMS01.MP_USERS (USERS_NAME,LOGIN_NAME,PASSWORD,EMPLOYEENO,EMAIL,TEL,LOCK_STATUS,USERS_STATUS,CREATEBY,CREATEDATE,UPDATEBY,UPDATEDATE) VALUES
  (:USERS_NAME,:LOGIN_NAME,:PASSWORD,:EMPLOYEENO,:EMAIL,:TEL,:LOCK_STATUS,:USERS_STATUS,sysdate,sysdate,sysdate,sysdate)
  `
	const bindParams = {}
	bindParams["USERS_NAME"] = username
	bindParams["LOGIN_NAME"] = loginname
	bindParams["PASSWORD"] = password
	bindParams["EMPLOYEENO"] = "900000"
	bindParams["EMAIL"] = email
	bindParams["TEL"] = tel
	bindParams["LOCK_STATUS"] = 0
	bindParams["USERS_STATUS"] = 1

	const result = await conn.execute(sql, bindParams)

	return result
}
