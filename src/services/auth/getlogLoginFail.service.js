module.exports = async (conn, login_name) => {
	const sql = ` SELECT count(*) "count"
  FROM MP_LOG_LOGIN_FAIL
  WHERE
  lower(login_name) = lower(:login_name)
  `
	const bindParams = {}
	bindParams["login_name"] = login_name

	const result = await conn.execute(sql, bindParams)

	return result.rows[0]
}
