module.exports = {
	get getUser() {
		return require("./getUser.service")
	},
	get saveUser() {
		return require("./saveUser.service")
	},
	get logLogin() {
		return require("./logLogin.service")
	},
	get setlogLoginFail() {
		return require("./setlogLoginFail.service")
	},
	get getlogLoginFail() {
		return require("./getlogLoginFail.service")
	},
}
