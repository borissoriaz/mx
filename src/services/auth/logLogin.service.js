module.exports = async (conn, USERS_ID = null, IPADDRESS = null, SESSIONID = null) => {
	const sql = ` INSERT INTO MP_LOG_LOGIN (USERS_ID,IPADDRESS,SESSIONID,STARTDATE,ENDDATE) VALUES
    (:USERS_ID,:IPADDRESS,:SESSIONID,sysdate,sysdate)

  `
	const bindParams = {}
	bindParams["USERS_ID"] = USERS_ID
	bindParams["IPADDRESS"] = IPADDRESS
	bindParams["SESSIONID"] = SESSIONID

	const result = await conn.execute(sql, bindParams)

	return result
}
