module.exports = async (conn, login_name) => {
	const sql = ` SELECT DISTINCT
	mu.USERS_ID,
	mu.PASSWORD,
	mu.USERS_NAME ,
	mu.LOGIN_NAME ,
	mu.EMPLOYEENO ,
	mu.EMAIL,
	mu.TEL,
	muid.DEPARTMENT_ID,
	muig.USERS_GROUP_ID,
	mugl.LEVELNO
FROM
	MP_USERS mu
LEFT JOIN MP_USERS_IN_DEPT muid ON
	muid.USERS_ID = mu.USERS_ID
LEFT JOIN MP_USERS_IN_GROUP muig ON
	muig.USERS_ID = mu.USERS_ID
LEFT JOIN MP_USERS_IN_LEVEL muil ON
	muil.USERS_ID = mu.USERS_ID
LEFT JOIN MP_USERS_GROUP_LEVEL mugl ON
	mugl.GROUP_LEVEL_ID = muil.GROUP_LEVEL_ID
WHERE
  lower(mu.login_name) = lower(:login_name)
  `
	const bindParams = {}
	bindParams["login_name"] = login_name

	const result = await conn.execute(sql, bindParams)
	console.log(result)

	return result.rows[0]
}
