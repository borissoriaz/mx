module.exports = async (conn, LOGIN_NAME = null, IPADDRESS = null, SESSIONID = null) => {
	const sql = ` INSERT INTO MP_LOG_LOGIN_FAIL (LOGIN_NAME,IPADDRESS,SESSIONID,LOGIN_TIME) VALUES
    (:LOGIN_NAME,:IPADDRESS,:SESSIONID,sysdate)

  `
	const bindParams = {}
	bindParams["LOGIN_NAME"] = LOGIN_NAME
	bindParams["IPADDRESS"] = IPADDRESS
	bindParams["SESSIONID"] = SESSIONID

	const result = await conn.execute(sql, bindParams)

	return result
}
