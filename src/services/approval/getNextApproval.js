module.exports = async (conn, approval_type, department_id, job_position_id, seq_no) => {
	let subsql = ""
	let additionalWhere = ""

	if (approval_type == 2) {
		subsql += ` union all
        select users_group_id,users_group_name,levelno,levelname,users_id,users_name,orderapp
        from(
        select distinct ug.users_group_id,ug.users_group_name,ugl.levelno,ugl.levelname,musers.users_id,musers.users_name, 3 as orderapp
        from mp_users_group ug
        inner join mp_users_in_group uig on ug.users_group_id = uig.users_group_id
        inner join mp_users musers on musers.users_id = uig.users_id
        inner join mp_users_group_level ugl on ugl.users_group_id = ug.users_group_id
        inner join mp_users_in_level uil on uil.group_level_id = ugl.group_level_id and uil.users_id = musers.users_id
        and ug.users_group_id = 2
        ) allmd`
	}

	// if (type == "NEXT") {
	// 	additionalWhere += `and seq_no > ${seq_no} `
	// }

	// if (type == "SPECIFIC") {
	// 	additionalWhere += `seq_no = ${seq_no} `
	// }

	additionalWhere += ` and SEQ_NO > ${seq_no} `

	const sql = `
	SELECT SEQ_NO,users_group_id,users_group_name,levelno,levelname,users_id,users_name,orderapp FROM (SELECT rownum "SEQ_NO",users_group_id,users_group_name,levelno,levelname,users_id,users_name,orderapp FROM (SELECT * FROM (select users_group_id,users_group_name,levelno,levelname,users_id,users_name,orderapp
        from(select distinct ug.users_group_id,ug.users_group_name,ugl.levelno,ugl.levelname,musers.users_id,musers.users_name, 1 as orderapp
        from MP_JOB_POSITION jp
        inner join mp_department dept on dept.department_id = jp.department_id
        inner join mp_users_in_dept muid on muid.department_id = dept.department_id
        inner join mp_users musers on musers.users_id = muid.users_id
        inner join mp_users_in_group uig on uig.users_id = muid.users_id
        inner join mp_users_group ug on ug.users_group_id = uig.users_group_id
        inner join mp_users_group_level ugl on ugl.users_group_id = ug.users_group_id
        inner join mp_users_in_level uil on uil.group_level_id = ugl.group_level_id and uil.users_id = musers.users_id
        where jp.department_id = ${department_id}
        and job_position_id = ${job_position_id}
        and ug.users_group_id = 5
        ) alldept
        
        union all
        select users_group_id,users_group_name,levelno,levelname,users_id,users_name,orderapp
        from(
        select distinct ug.users_group_id,ug.users_group_name,ugl.levelno,ugl.levelname,musers.users_id,musers.users_name, 2 as orderapp
        from MP_JOB_POSITION jp
        inner join mp_department dept on dept.department_id = jp.department_id
        inner join mp_users_in_dept muid on muid.department_id = dept.department_id
        inner join mp_users musers on musers.users_id = muid.users_id
        inner join mp_users_in_group uig on uig.users_id = muid.users_id
        inner join mp_users_group ug on ug.users_group_id = uig.users_group_id
        inner join mp_users_group_level ugl on ugl.users_group_id = ug.users_group_id
        inner join mp_users_in_level uil on uil.group_level_id = ugl.group_level_id and uil.users_id = musers.users_id
        where jp.department_id = ${department_id}
        and job_position_id = ${job_position_id}
        and ug.users_group_id = 4
        ) allhr
        ${subsql}
        )
        order by orderapp,levelno
        )) WHERE 1=1 
        ${additionalWhere}
         order by SEQ_NO ASC
        

	`
	console.log(sql)

	const result = await conn.execute(sql)
	const flowTemplateResult = result.rows

	return flowTemplateResult
}
