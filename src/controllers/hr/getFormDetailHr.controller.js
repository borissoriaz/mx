const services = require("../../services")
const lib = require("../../lib")
const Errorly = require("../../lib/response/Errorly")

module.exports = async (req, res) => {
	const { request_job_id } = req.query
	const conn = await req.conn
	const user_id = req.decodedToken.USERS_ID

	let subsql = ""

	// if (!!request_job_id) {
	// 	subsql += ` `
	// }

	const sqlJobRequest = `
	SELECT 
	mprj.*,
		mpdpt.DEPARTMENT_NAME,
		mpdi.DIVISION_NAME,
		mpjp.JOB_POSITION_NAME,
		mpp.PLANTSHORTNAME,
		mps.SECTION_NAME,
		mpedu.EDUCATION_TYPE_NAME,
		mpjt.approve_type
	FROM mp_request_job mprj
	LEFT JOIN mp_plant mpp ON
		mpp.plantno = mprj.plantno
	LEFT JOIN mp_section mps ON
		mps.section_id = mprj.section_id
	LEFT JOIN mp_department mpdpt ON
		mpdpt.department_id = mprj.department_id
	LEFT JOIN mp_division mpdi ON
		mpdi.division_id = mprj.division_id
	LEFT JOIN mp_job_position mpjp ON
		mpjp.job_position_id = mprj.job_position_id
	LEFT JOIN mp_job_type mpjt ON
		mpjt.job_type_id = mpjp.job_type_id
	LEFT JOIN mp_education_type mpedu ON
	mpedu.education_type_id = mprj.education_type_id

WHERE 1=1
and request_job_id = ${request_job_id}
ORDER BY
	request_job_id

	`

	const resultJobRequest = await conn.execute(sqlJobRequest)

	const newEmpListSql = `
	SELECT 
	*
FROM
	MP_EMP_NEW new
LEFT JOIN MP_EMP_NEW_LIST newl ON newl.emp_new_id = new.emp_new_id
WHERE 1=1
and new.request_job_id = ${request_job_id}
ORDER BY
new.request_job_id
	`

	const newEmpListResult = await conn.execute(newEmpListSql)

	const response = resultJobRequest.rows[0]
	response["emp_new_list_item"] = newEmpListResult.rows

	console.log(response)

	res.resly(200, { data: response })
}

//
