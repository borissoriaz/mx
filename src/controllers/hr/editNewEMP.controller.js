const services = require("../../services")
const lib = require("../../lib")
const bcrypt = require("bcrypt")
const Errorly = require("../../lib/response/Errorly")

module.exports = async (req, res) => {
	const conn = await req.conn
	const body = req.body
	console.log(body)
	const users_id = req.decodedToken.USERS_ID

	const updateNewEMPSql = `

	UPDATE mp_emp_new
    SET 
        total_amount = to_number(${body.newEmpBank.length})
	WHERE
		request_job_id = to_number(${body.request_job_id})

    `
	console.log("updateNewEMPSql", updateNewEMPSql)

	const updateNewEMPSqlResult = await conn.execute(updateNewEMPSql)

	if (body.newEmpBank.length > 0) {
		const deleteEMP = `
        DELETE FROM mp_emp_new_list
        WHERE emp_new_id=to_number('${body.emp_new_id}')
        `
		const result = await conn.execute(deleteEMP)

		const insertNewEMPListSql = `
    INSERT INTO
	mp_emp_new_list (
	emp_new_id,
	emp_name,
	start_date,
	list_status,
	createby,
	createdate,
	updateby,
	updatedate,
	remark
)values(
	to_number('${body.emp_new_id}'),
	:emp_name,
	to_date(:start_date,'dd/mm/yyyy'),
	to_number(:list_status),
    to_number(${users_id}),
    sysdate,
    NULL,
    NULL,
	:remark)
    `

		const bindParams = body.newEmpBank

		console.log("insertNewEMPListSql", insertNewEMPListSql, bindParams)

		const insertNewEMPListResult = await conn.executeMany(insertNewEMPListSql, bindParams)
	}

	await conn.commit()

	res.resly(200, {})
}
