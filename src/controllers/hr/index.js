module.exports = {
	get getFormDetailHr() {
		return require("./getFormDetailHr.controller")
	},
	get saveNewEMP() {
		return require("./saveNewEMP.controller")
	},
	get editNewEMP() {
		return require("./editNewEMP.controller")
	},
}
