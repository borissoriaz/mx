const services = require("../../services")
const lib = require("../../lib")
const bcrypt = require("bcrypt")
const Errorly = require("../../lib/response/Errorly")

module.exports = async (req, res) => {
	const { username, password } = req.body
	const ip = req.headers["x-forwarded-for"] || req.socket.remoteAddress

	const conn = await req.conn

	const userData = await services.auth.getUser(conn, username)

	if (!userData) {
		throw new Errorly(404, "ไม่พบผู้ใช้นี้ในระบบ")
	}

	const failLoginCount = await services.auth.getlogLoginFail(conn, userData.LOGIN_NAME)

	const match = await bcrypt.compare(password, userData.PASSWORD)

	if (!match) {
		await services.auth.setlogLoginFail(conn, userData.LOGIN_NAME, ip, req.rqid)
		await conn.commit()
		if (failLoginCount.count >= 3) {
			await checkFailCount(conn, userData)
			throw new Errorly(401, "การเข้าระบบถูกล็อค กรุณาติดต่อผู้ดูแลระบบ")
		} else {
			throw new Errorly(401, "Username or Password incorrect")
		}
	}

	if (failLoginCount.count >= 3) {
		await checkFailCount(conn, userData)
	}

	delete userData.PASSWORD

	const payload = userData
	const accessToken = lib.token.signToken(payload)

	await services.auth.logLogin(conn, userData.USERS_ID, ip, req.rqid)

	await conn.commit()

	res.resly(200, { data: { userData: userData, accessToken } })
}

const checkFailCount = async (conn, userData) => {
	const sql = `
		UPDATE 
			MP_USERS 
		SET 
			lock_status = 1
		WHERE 
			login_name = '${userData.LOGIN_NAME}'
		`

	await conn.execute(sql)
	await conn.commit()
	throw new Errorly(401, "การเข้าระบบถูกล็อค กรุณาติดต่อผู้ดูแลระบบ")
}
