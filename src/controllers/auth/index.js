module.exports = {
  get login() {
    return require("./login.controller")
  },
  get signup() {
    return require("./signup.controller")
  },
}
