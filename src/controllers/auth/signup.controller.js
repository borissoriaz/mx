const services = require("../../services")
const lib = require("../../lib")
const Errorly = require("../../lib/response/Errorly")
const bcrypt = require("bcrypt")
module.exports = async (req, res) => {
	const { username, loginname, password, email, tel } = req.body
	const conn = await req.conn

	const userData = await services.auth.getUser(conn, username)

	if (userData) {
		throw new Errorly(409, "User alredy exist")
	}

	const hash = await bcrypt.hash(password, 10)

	const result = await services.auth.saveUser(conn, username, loginname, hash, email, tel)

	await conn.commit()

	res.resly(200, {})
}
