const services = require("../../services")
const lib = require("../../lib")
const bcrypt = require("bcrypt")
const Errorly = require("../../lib/response/Errorly")

module.exports = async (req, res) => {
	const { key, params } = req.body
	const conn = await req.conn
	let result = []
	let sql = ""
	const bindParams = {}
	let subSql = ""

	console.log(req.body)
	switch (key) {
		case "plant":
			sql = `SELECT * FROM mp_plant WHERE 1=1`
			break
		case "division":
			if (params.getPlantNO) {
				subSql = ` and mdv.plantno=:getPlantNO`
				bindParams["getPlantNO"] = params.getPlantNO?.value
			}
			sql = `SELECT * FROM mp_division mdv LEFT JOIN MP_PLANT mpt ON mpt.PLANTNO = mdv.PLANTNO  WHERE 1=1 ${subSql} order by division_id`
			break
		case "section":
			if (params.init) {
				subSql = ` and department_id=:getdeptID`
				bindParams["getdeptID"] = params.getdeptID?.value
			}
			sql = `SELECT * FROM mp_section WHERE 1=1 ${subSql} order by section_id`
			break

		case "department":
			if (params.init) {
				subSql = `and division_id=:getDivisionID and plantno=:getPlantNO`
				bindParams["getDivisionID"] = params.getDivisionID?.value
				bindParams["getPlantNO"] = params.getPlantNO?.value
			}
			sql = `select * from mp_department where 1=1 ${subSql} order by department_id`
			break

		case "position":
			if (params.init) {
				subSql = `and section_id=:getsectionID and department_id=:getdeptID`
				bindParams["getsectionID"] = params.getsectionID?.value
				bindParams["getdeptID"] = params.getdeptID?.value
			}
			sql = `select * from mp_job_position where 1=1 ${subSql} order by job_position_id`
			break

		case "posDate":
			if (params.init) {
				subSql = `and mjp.job_position_id=:getposID`
				bindParams["getposID"] = params.getposID?.value
			}
			sql = `select mjc.limitdays "limitdays" from mp_job_position mjp inner join mp_job_condition mjc on mjp.job_type_id = mjc.job_type_id where 1=1 ${subSql}`

			break
		case "quota":
			sql = `select * from mp_quota where department_id = ${params.getdeptID?.value} and sysdate >= effectivedate and rownum =1 order by quota_id desc`
			break

		case "education":
			sql = `SELECT * FROM mp_education_type WHERE 1=1`
			break

		default:
			throw new Errorly(404, "No Label Found")
			break
	}
	console.log(sql)

	result = await conn.execute(sql, bindParams)

	res.resly(200, { data: result.rows })
}

//
