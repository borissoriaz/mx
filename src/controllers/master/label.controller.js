const services = require("../../services")
const lib = require("../../lib")
const bcrypt = require("bcrypt")
const Errorly = require("../../lib/response/Errorly")

module.exports = async (req, res) => {
	const { key, params } = req.body
	const conn = await req.conn
	let result = []
	let sql = ""
	const bindParams = {}
	let subSql = ""

	console.log(req.body)
	switch (key) {
		case "plant":
			sql = `SELECT PLANTNO "value", PLANTSHORTNAME "label" FROM mp_plant WHERE 1=1 order by PLANTSHORTNAME`
			break
		case "division":
			if (params.getPlantNO?.value) {
				subSql = ` and plantno=:getPlantNO`
				bindParams["getPlantNO"] = params.getPlantNO?.value
			}
			sql = `SELECT division_id "value", division_name "label" FROM mp_division WHERE 1=1 ${subSql} order by division_name`
			break
		case "section":
			if (params.getdeptID?.value) {
				subSql = ` and department_id=:getdeptID`
				bindParams["getdeptID"] = params.getdeptID?.value
			}
			sql = `SELECT section_id "value", section_name "label" FROM mp_section WHERE 1=1 ${subSql} order by section_name`
			break

		case "department":
			if (params.init) {
				subSql = ` and division_id=:getDivisionID and plantno=:getPlantNO`
				bindParams["getDivisionID"] = params.getDivisionID?.value
				bindParams["getPlantNO"] = params.getPlantNO?.value
			}
			sql = `select department_id "value", department_name "label", costcenter "costcenter" from mp_department where 1=1 ${subSql} order by department_id`
			break

		case "position":
			if (params.init) {
				subSql = ` and section_id=:getsectionID and department_id=:getdeptID`
				bindParams["getsectionID"] = params.getsectionID?.value
				bindParams["getdeptID"] = params.getdeptID?.value
			}
			sql = `select job_position_id "value", job_position_name "label" from mp_job_position where 1=1 ${subSql} order by job_position_id`
			break

		case "posDate":
			if (params.init) {
				subSql = ` and mjp.job_position_id=:getposID`
				bindParams["getposID"] = params.getposID?.value
			}
			sql = `select mjc.limitdays "limitdays" from mp_job_position mjp inner join mp_job_condition mjc on mjp.job_type_id = mjc.job_type_id where 1=1 ${subSql}`

			break
		case "quota":
			sql = `select amount "amount" from mp_quota where department_id = ${params.getdeptID?.value} and sysdate >= effectivedate and rownum =1 order by quota_id desc`
			break

		case "education":
			sql = `SELECT education_type_id "value", education_type_name "label" FROM mp_education_type WHERE 1=1`
			break

		case "manAct":
			sql = `select 
			case substr(costct,1,2)
						   when '10' then 'SAB'
						   when '11' then 'SAB2'
						   when '20' then 'SLAB'
						   when '21' then 'SRAB'
						   when '22' then 'SLAB2'
						   when '30' then 'SRDC'
						   when '40' then 'SATc'
						   when '60' then 'SDC'
						   when '90' then 'SC1'
						   when '80' then 'SAM'
						   when '91' then 'JMAX'
						   else min(codcomp1)
						end plant
		   ,dept,substr(costct,1,5) costdept,count(codempid) emp_mp
		   ,costct
	   		from hrms.v_emp01 
	   		where staemp!='9' 
	   		and costct = '${params.getdeptID?.costcenter}' 
	   		and numlvl <12
	   		group by substr(costct,1,2),dept,substr(costct,1,5),costct`
			break

		default:
			throw new Errorly(404, "No Label Found")
			break
	}
	console.log(sql)

	result = await conn.execute(sql, bindParams)

	res.resly(200, { data: result.rows })
}

//
