module.exports = {
	get plant() {
		return require("./plant.controller")
	},
	get department() {
		return require("./department.controller")
	},
	get jobType() {
		return require("./jobType.controller")
	},
	get position() {
		return require("./position.controller")
	},
	get label() {
		return require("./label.controller")
	},
	get all() {
		return require("./all.controller")
	},
	get add() {
		return require("./add.controller")
	},
	get update() {
		return require("./update.controller")
	},
	get delete() {
		return require("./delete.controller")
	},
}
