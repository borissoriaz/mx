const services = require("../../services")
const lib = require("../../lib")
const Errorly = require("../../lib/response/Errorly")

module.exports = async (req, res) => {
	const {
		seq_no,
		department_id,
		job_position_id,
		job_request_id,
		approval_type,
		approval_hist_id,
	} = req.body
	const conn = await req.conn
	const users_id = req.decodedToken.USERS_ID
	let APPROVE_STATUS_DEFAULT = "ON APPROVAL"

	console.log("req.body", req.body)

	///////////////////////////////////////////////
	const updateAppHistSql = `
	UPDATE MP_APPROVAL_HIST
    SET 
	approval_status = 'APPROVED',
	approveddate = sysdate,
	users_id = ${users_id} 
    WHERE approval_hist_id = ${approval_hist_id}
	`

	console.log("updateAppHistSql", updateAppHistSql)

	const updateAppHistResult = await conn.execute(updateAppHistSql)

	//////////////////////////////////////////////////////

	const nextApprovalData = await services.approval.getNextApproval(
		conn,
		approval_type,
		department_id,
		job_position_id,
		seq_no
	)

	console.log("nextApprovalData", nextApprovalData)

	let nextApporval = nextApprovalData[0]

	if (nextApprovalData.length === 0) {
		// throw new Errorly("no next apporval go to hr new emp")
		APPROVE_STATUS_DEFAULT = "COMPLETED"

		const selectPreviousApprovalSql = `
		SELECT *
		FROM MP_APPROVAL_HIST
		WHERE approval_hist_id = ${approval_hist_id}
		`

		const selectPreviousApprovalResult = await conn.execute(selectPreviousApprovalSql)

		console.log(selectPreviousApprovalResult)

		nextApporval = selectPreviousApprovalResult.rows[0]

		nextApporval["SEQ_NO"] = 9999
		nextApporval["USERS_GROUP_ID"] = 9999
	}

	////////////////////////////////////////////////

	const insertApprovalHistSql = `
	INSERT INTO	MP_APPROVAL_HIST (
	REQUEST_JOB_ID,
	SEQ_NO,
	USERS_GROUP_ID,
	LEVELNO,
	USERS_ID,
	APPROVAL_STATUS,
	CREATEDATE,
	CREATEBY,
	APPROVEDDATE,
	REMARK
	)VALUES(
	${job_request_id},
	${nextApporval.SEQ_NO},
	${nextApporval.USERS_GROUP_ID},
	${nextApporval.LEVELNO},
	NULL,
	'${APPROVE_STATUS_DEFAULT}',
	sysdate,
	${users_id},
	NULL,
	NULL
	)
	`
	console.log("insertApprovalHistSql", insertApprovalHistSql)

	const insertApprovalHistResult = await conn.execute(insertApprovalHistSql)

	const updateRequestFormSql = `
	UPDATE mp_request_job
    SET flow_status = '${APPROVE_STATUS_DEFAULT}',
    approval_hist_id = to_number(( SELECT approval_hist_id FROM MP_APPROVAL_HIST WHERE rowid = '${insertApprovalHistResult.lastRowid}')),
	latest_approved_hist_id = ${approval_hist_id}
    WHERE request_job_id = ${job_request_id}
	`

	console.log("updateRequestFormSql", updateRequestFormSql)

	const updateRequestFormResult = await conn.execute(updateRequestFormSql)

	// console.log("nextApprovalData", nextApprovalData)

	await conn.commit()

	res.resly(200, { data: updateRequestFormResult })
}
