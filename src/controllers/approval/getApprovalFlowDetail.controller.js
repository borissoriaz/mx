const services = require("../../services")
const lib = require("../../lib")
const Errorly = require("../../lib/response/Errorly")

module.exports = async (req, res) => {
	const { seq_no, approval_type, department_id, job_position_id, type, request_job_id } =
		req.query
	const conn = await req.conn

	console.log("req", req.query)

	let subsql = ""
	let additionalWhere = ""

	if (approval_type == 2) {
		subsql += ` union all
        select users_group_id,users_group_name,levelno,levelname,users_id,users_name,orderapp
        from(
        select distinct ug.users_group_id,ug.users_group_name,ugl.levelno,ugl.levelname,musers.users_id,musers.users_name, 3 as orderapp
        from mp_users_group ug
        inner join mp_users_in_group uig on ug.users_group_id = uig.users_group_id
        inner join mp_users musers on musers.users_id = uig.users_id
        inner join mp_users_group_level ugl on ugl.users_group_id = ug.users_group_id
        inner join mp_users_in_level uil on uil.group_level_id = ugl.group_level_id and uil.users_id = musers.users_id
        and ug.users_group_id = 2
        ) allmd`
	}

	if (type == "NEXT") {
		additionalWhere += ` and seq_no > ${seq_no} `
	}

	if (type == "SPECIFIC") {
		additionalWhere += ` seq_no = ${seq_no} `
	}

	const sql = `
	SELECT SEQ_NO,users_group_id,users_group_name,levelno,levelname,users_id,users_name,orderapp FROM (SELECT rownum "SEQ_NO",users_group_id,users_group_name,levelno,levelname,users_id,users_name,orderapp FROM (SELECT * FROM (select users_group_id,users_group_name,levelno,levelname,users_id,users_name,orderapp
        from(select distinct ug.users_group_id,ug.users_group_name,ugl.levelno,ugl.levelname,musers.users_id,musers.users_name, 1 as orderapp
        from MP_JOB_POSITION jp
        inner join mp_department dept on dept.department_id = jp.department_id
        inner join mp_users_in_dept muid on muid.department_id = dept.department_id
        inner join mp_users musers on musers.users_id = muid.users_id
        inner join mp_users_in_group uig on uig.users_id = muid.users_id
        inner join mp_users_group ug on ug.users_group_id = uig.users_group_id
        inner join mp_users_group_level ugl on ugl.users_group_id = ug.users_group_id
        inner join mp_users_in_level uil on uil.group_level_id = ugl.group_level_id and uil.users_id = musers.users_id
        where jp.department_id = ${department_id}
        and job_position_id = ${job_position_id}
        and ug.users_group_id = 5
        ) alldept
        
        union all
        select users_group_id,users_group_name,levelno,levelname,users_id,users_name,orderapp
        from(
        select distinct ug.users_group_id,ug.users_group_name,ugl.levelno,ugl.levelname,musers.users_id,musers.users_name, 2 as orderapp
        from MP_JOB_POSITION jp
        inner join mp_department dept on dept.department_id = jp.department_id
        inner join mp_users_in_dept muid on muid.department_id = dept.department_id
        inner join mp_users musers on musers.users_id = muid.users_id
        inner join mp_users_in_group uig on uig.users_id = muid.users_id
        inner join mp_users_group ug on ug.users_group_id = uig.users_group_id
        inner join mp_users_group_level ugl on ugl.users_group_id = ug.users_group_id
        inner join mp_users_in_level uil on uil.group_level_id = ugl.group_level_id and uil.users_id = musers.users_id
        where jp.department_id = ${department_id}
        and job_position_id = ${job_position_id}
        and ug.users_group_id = 4
        ) allhr
        ${subsql}
        )
        order by orderapp,levelno
        )) WHERE 1=1 
        order by SEQ_NO desc
        

	`
	console.log(sql)

	const result = await conn.execute(sql)
	const flowTemplateResult = result.rows

	////////////////////////////////////////////////
	const sql2 = `
	SELECT
	mah.APPROVAL_HIST_ID ,
	mah.SEQ_NO ,
	mug.USERS_GROUP_NAME ,
	LEVELNO,
	mu.USERS_NAME,
	mah.APPROVEDDATE,
	mah.APPROVAL_STATUS,
	mah.REMARK
FROM
	MP_APPROVAL_HIST mah
LEFT JOIN MP_USERS_GROUP mug ON
	mug.USERS_GROUP_ID = mah.USERS_GROUP_ID
LEFT JOIN MP_USERS_GROUP_LEVEL mugl ON
	mugl.GROUP_LEVEL_ID = mah.LEVELNO
LEFT JOIN MP_USERS mu ON
	mu.USERS_ID = mah.USERS_ID
WHERE
	mah.REQUEST_JOB_ID  = ${request_job_id}
ORDER BY mah.APPROVAL_HIST_ID 
        

	`
	console.log(sql2)

	const flowHistoryResult = await conn.execute(sql2)

	/////////////////////////////////
	// const finalResult = []
	flowTemplateResult.map((item) => {
		const filtedHistory = flowHistoryResult.rows.filter(
			(element) => element.SEQ_NO == item.SEQ_NO
		)

		item["APPROVALDATE"] = filtedHistory[0]?.APPROVEDDATE || null
		item["APPROVAL_STATUS"] = filtedHistory[0]?.APPROVAL_STATUS || null
		item["REMARK"] = filtedHistory[0]?.REMARK || ""
		item["WHO"] = filtedHistory[0]?.USERS_NAME || ""

		return item
	})

	res.resly(200, { data: flowTemplateResult })
}

//
