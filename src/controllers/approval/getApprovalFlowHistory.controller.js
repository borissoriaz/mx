const services = require("../../services")
const lib = require("../../lib")
const Errorly = require("../../lib/response/Errorly")

module.exports = async (req, res) => {
	const { request_job_id } = req.query
	const conn = await req.conn

	let subsql = ""
	let additionalWhere = ""

	const sql = `
	SELECT
	mah.APPROVAL_HIST_ID ,
	mah.SEQ_NO ,
	mug.USERS_GROUP_NAME ,
	LEVELNO,
	mu.USERS_NAME,
	mah.APPROVEDDATE,
	mah.APPROVAL_STATUS
FROM
	MP_APPROVAL_HIST mah
LEFT JOIN MP_USERS_GROUP mug ON
	mug.USERS_GROUP_ID = mah.USERS_GROUP_ID
LEFT JOIN MP_USERS_GROUP_LEVEL mugl ON
	mugl.GROUP_LEVEL_ID = mah.LEVELNO
LEFT JOIN MP_USERS mu ON
	mu.USERS_ID = mah.USERS_ID
WHERE
	mah.REQUEST_JOB_ID  = ${request_job_id}
ORDER BY mah.APPROVAL_HIST_ID 
        

	`
	console.log(sql)

	const result = await conn.execute(sql)

	res.resly(200, { data: result.rows })
}

//
