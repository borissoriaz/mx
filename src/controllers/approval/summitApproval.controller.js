const services = require("../../services")
const lib = require("../../lib")
const Errorly = require("../../lib/response/Errorly")

module.exports = async (req, res) => {
	const { department_id, job_position_id, job_request_id, approval_type } = req.body
	const conn = await req.conn
	const users_id = req.decodedToken.USERS_ID

	console.log("req.body", req.body)

	const nextApprovalData = await services.approval.getNextApproval(
		conn,
		approval_type,
		department_id,
		job_position_id,
		0
	)

	if (nextApprovalData.length == 0) {
		throw new Errorly("no next apporval")
	}

	const nextApporval = nextApprovalData[0]

	const insertApprovalHistSql = `
	INSERT INTO	MP_APPROVAL_HIST (
	REQUEST_JOB_ID,
	SEQ_NO,
	USERS_GROUP_ID,
	LEVELNO,
	USERS_ID,
	APPROVAL_STATUS,
	CREATEDATE,
	CREATEBY,
	APPROVEDDATE,
	REMARK
	)VALUES(
	${job_request_id},
	${nextApporval.SEQ_NO},
	${nextApporval.USERS_GROUP_ID},
	${nextApporval.LEVELNO},
	NULL,
	'ON APPROVAL',
	sysdate,
	${users_id},
	NULL,
	NULL
	)
	`
	console.log("insertApprovalHistSql", insertApprovalHistSql)

	const insertApprovalHistResult = await conn.execute(insertApprovalHistSql)

	const updateRequestFormSql = `
	UPDATE mp_request_job
    SET flow_status = 'ON APPROVAL',
    approval_hist_id = to_number(( SELECT approval_hist_id FROM MP_APPROVAL_HIST WHERE rowid = '${insertApprovalHistResult.lastRowid}'))
    WHERE request_job_id = ${job_request_id}
	`

	console.log("updateRequestFormSql", updateRequestFormSql)

	const updateRequestFormResult = await conn.execute(updateRequestFormSql)

	// console.log("nextApprovalData", nextApprovalData)

	await conn.commit()

	res.resly(200, { data: updateRequestFormResult })
}
