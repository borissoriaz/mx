module.exports = {
	get getApprovalFlowDetail() {
		return require("./getApprovalFlowDetail.controller")
	},
	get getApprovalFlow() {
		return require("./getApprovalFlow.controller")
	},
	get getApprovalFlowHistory() {
		return require("./getApprovalFlowHistory.controller")
	},
	get summitApproval() {
		return require("./summitApproval.controller")
	},
	get getApprovalFlowList() {
		return require("./getApprovalFlowList.controller")
	},
	get holdApproval() {
		return require("./holdApproval.controller")
	},
	get doApproval() {
		return require("./doApproval.controller")
	},
	get rejectApproval() {
		return require("./rejectApproval.controller")
	},
	get getHrFlowList() {
		return require("./getHrFlowList.controller")
	},
	get reApproval() {
		return require("./reApproval.controller")
	},
}
