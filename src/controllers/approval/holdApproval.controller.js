const services = require("../../services")
const lib = require("../../lib")
const Errorly = require("../../lib/response/Errorly")

module.exports = async (req, res) => {
	const { approval_hist_id_item, remark } = req.body
	const conn = await req.conn

	console.log(" req.body req.body", req.body)

	const bindParams = approval_hist_id_item.map((item) => ({ approval_hist_id: item }))

	/////////////////////////////////////////////////
	const updateAppHistSql = `
	UPDATE MP_APPROVAL_HIST
    SET approval_status = 'ON HOLD',
	remark ='${remark}'
	approveddate = sysdate
    WHERE approval_hist_id = :approval_hist_id
	`

	console.log("updateAppHistSql", updateAppHistSql, bindParams)

	const updateAppHistResult = await conn.executeMany(updateAppHistSql, bindParams)

	const updateReqJobtSql = `
	UPDATE MP_REQUEST_JOB
    SET flow_status = 'ON HOLD'
	latest_approved_hist_id = :approval_hist_id
    WHERE approval_hist_id = :approval_hist_id
	`

	console.log("updateReqJobtSql", updateReqJobtSql)

	const updateRequestFormResult = await conn.executeMany(updateReqJobtSql, bindParams)

	// console.log("nextApprovalData", nextApprovalData)

	await conn.commit()

	res.resly(200, { data: updateRequestFormResult })
}
