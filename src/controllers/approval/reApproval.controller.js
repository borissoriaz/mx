const services = require("../../services")
const lib = require("../../lib")
const Errorly = require("../../lib/response/Errorly")

module.exports = async (req, res) => {
	const { approval_hist_id } = req.body
	const conn = await req.conn

	console.log(" req.body req.body", req.body)

	/////////////////////////////////////////////////
	const updateAppHistSql = `
	UPDATE MP_APPROVAL_HIST
    SET approval_status = 'ON APPROVAL',
    WHERE approval_hist_id = ${approval_hist_id}
	`

	const updateAppHistResult = await conn.execute(updateAppHistSql)

	const updateReqJobtSql = `
	UPDATE MP_REQUEST_JOB
    SET flow_status = 'ON APPROVAL'
    WHERE approval_hist_id = ${approval_hist_id}
	`

	console.log("updateReqJobtSql", updateReqJobtSql)

	const updateRequestFormResult = await conn.execute(updateReqJobtSql)

	// console.log("nextApprovalData", nextApprovalData)

	await conn.commit()

	res.resly(200, { data: updateRequestFormResult })
}
