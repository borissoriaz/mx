const services = require("../../services")
const lib = require("../../lib")
const bcrypt = require("bcrypt")
const Errorly = require("../../lib/response/Errorly")

module.exports = async (req, res) => {
	const conn = await req.conn
	const body = req.body
	console.log("dddddddddd", body)
	const users_id = req.decodedToken.USERS_ID
	const sql = `

    UPDATE mp_request_job
    SET 
        job_position_id = to_number(${body.job_position_id}),
        plantno = to_number(${body.plantno}),
        division_id = to_number(${body.division_id}),
        department_id = to_number(${body.department_id}),
        section_id = to_number(${body.section_id}),
        tel = '${body.tel}',
        request_amount = to_number(${body.request_amount}),
        request_date = TO_DATE('${body.request_date}', 'dd/mm/yyyy'),
        request_type = to_number(${body.request_type}),
        remark = '${body.remark}',
        education_type_id = to_number(${body.education_type_id}),
        education_other = NULL,
        gender=to_number(${body.gender}),
        age=to_number(${body.age}),
        shiftwork=to_number(${body.shiftwork}),
        work_exp=to_number(${body.work_exp}),
        computer_skill='${body.computer_skill}',
        talent='${body.talent}',
        updateby=to_number(${users_id}),
        updatedate=sysdate,
        man_act= to_number(${body.man_act}),
        dept_quota=to_number(${body.dept_quota}),
        excelflag=to_number(${body.excelflag}),
        pointflag=to_number(${body.pointflag})
    WHERE request_job_id = '${body.request_job_id}'

     `

	console.log(sql)

	const result = await conn.execute(sql)

	if (req.body.job_replace.length > 0) {
		const deleteReqReplace = `
        DELETE FROM mp_request_replace
        WHERE request_job_id = '${body.request_job_id}'
        `
		const resultdeleteReqReplace = await conn.execute(deleteReqReplace)

		const replaceJobSql = `
    INSERT INTO
	mp_request_replace (
	request_job_id,
	resign_type,
	resign_date,
	emp_id,
	emp_position,
	emp_name,
	replace_status,
	createby,
	createdate,
	updateby,
	updatedate
)values(
    to_number(( SELECT request_job_id FROM MP_REQUEST_JOB mrj WHERE mrj.rowid = '${result.lastRowid}')),
    :resign_type,
    TO_DATE(:resign_date, 'dd/mm/yyyy'),
	:emp_id,
	:emp_position,
	:emp_name,
    1,
    to_number(${users_id}),
    sysdate,
    NULL,
    NULL)
    
    `

		const bindParams3 = req.body.job_replace

		console.log(replaceJobSql)
		console.log("bindParams3", bindParams3)

		const replaceJObResult = await conn.executeMany(replaceJobSql, bindParams3)
	}

	if (req.body.file_upload.length > 0) {
		const deleteReqFile = `
        DELETE FROM mp_request_job_upload
        WHERE request_job_id = '${body.request_job_id}'
        `
		const resultdeleteReqFile = await conn.execute(deleteReqFile)

		const fileUploadJobSql = `
    INSERT INTO
	mp_request_job_upload (
	request_job_id,
	upload_type,
	upload_name,
	upload_file,
	upload_status,
	createby,
	createdate,
	updateby,
	updatedate
)values(
    to_number(( SELECT request_job_id FROM MP_REQUEST_JOB mrj WHERE mrj.rowid = '${result.lastRowid}')),
    :upload_type,
    :upload_name,
	:upload_file,
    1,
    to_number(${users_id}),
    sysdate,
    NULL,
    NULL)
    
    `

		const fileUploadJobSqlbindParams = req.body.file_upload

		console.log(fileUploadJobSql)
		console.log("fileUploadJobSqlbindParams", fileUploadJobSqlbindParams)

		const fileUploadJobSqlResult = await conn.executeMany(
			fileUploadJobSql,
			fileUploadJobSqlbindParams
		)
	}

	const selectid = `
    SELECT request_job_id FROM MP_REQUEST_JOB mrj WHERE mrj.rowid = '${result.lastRowid}'
    `
	const reqJobIdData = await conn.execute(selectid)
	console.log(reqJobIdData)
	await conn.commit()

	res.resly(200, { data: reqJobIdData.rows })
}
