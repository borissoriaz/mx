module.exports = {
	get saveForm() {
		return require("./saveForm.controller")
	},
	get getForm() {
		return require("./getForm.controller")
	},
	get getFormDetail() {
		return require("./getFormDetail.controller")
	},
	get updateForm() {
		return require("./updateForm.controller")
	},
	get closeForm() {
		return require("./closeForm.controller")
	},
}
