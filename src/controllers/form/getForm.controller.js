const services = require("../../services")
const lib = require("../../lib")
const Errorly = require("../../lib/response/Errorly")

module.exports = async (req, res) => {
	const {
		request_job_id,
		request_status = 1,
		department_id,
		division_id,
		plantno,
	} = req.query
	const conn = await req.conn
	const user_id = req.decodedToken.USERS_ID

	console.log("req.query", req.query)
	let subsql = ""
	let childSql = ""
	let topChildSql = ""

	if (!!request_job_id) {
		subsql += `and request_job_id = ${request_job_id}`
	}

	if (!!request_status) {
		if (request_status == 1) {
			subsql += ` and flow_status <> 'CLOSE' `
		} else if (request_status == 2) {
			subsql += ` and flow_status = 'CLOSE' `
		}
	}

	if (!!department_id) {
		childSql += ` and department_id = ${department_id}`
	}

	if (!!division_id) {
		subsql += ` and division_id = ${division_id}`
	}

	if (!!plantno) {
		subsql += ` and plantno = ${plantno}`
	}

	const sql = `
	SELECT DISTINCT
	*
FROM
	(
	SELECT
	 
		mprj.*,
		mpu.USERS_NAME,
		mpdpt.DEPARTMENT_NAME,
		mpdi.DIVISION_NAME,
		mpjp.JOB_POSITION_NAME,
		ugl.LEVELNAME,
		usg.USERS_GROUP_NAME,
		usg.USERS_GROUP_NAME  || ',' ||ugl.LEVELNAME "LATEST_STATUS",
		mpux.USERS_NAME "APPROVTER_NAME",
		mpjt.approve_type,
		mpjc.alert_approve,
		mpjc.alert_close,
		mah.APPROVAL_STATUS
	FROM
		mp_request_job mprj
	LEFT JOIN mp_users mpu ON
		mpu.users_id = mprj.users_id
	LEFT JOIN mp_department mpdpt ON
		mpdpt.department_id = mprj.department_id
	LEFT JOIN mp_division mpdi ON
		mpdi.division_id = mprj.division_id
	LEFT JOIN mp_job_position mpjp ON
		mpjp.job_position_id = mprj.job_position_id
	LEFT JOIN mp_job_type mpjt ON
		mpjt.job_type_id = mpjp.job_type_id
	LEFT JOIN mp_job_condition mpjc ON
		mpjc.job_type_id = mpjt.job_type_id
	LEFT JOIN MP_APPROVAL_HIST mah ON mah.approval_hist_id  = mprj.latest_approved_hist_id
	LEFT JOIN mp_users_group_level ugl ON ugl.LEVELNO  = mah.LEVELNO 
	LEFT JOIN mp_users mpux ON mpux.users_id  = mah.USERS_ID
	LEFT JOIN mp_users_group usg ON usg.USERS_GROUP_ID  = mah.USERS_GROUP_ID 
	
	WHERE
		1 = 1
		AND mprj.users_id = ${user_id}
		AND mprj.flow_status = 'NEW'
UNION ALL
	SELECT
		mprj.*,
		mpu.USERS_NAME,
		mpdpt.DEPARTMENT_NAME,
		mpdi.DIVISION_NAME,
		mpjp.JOB_POSITION_NAME,
		ugl.LEVELNAME,
		usg.USERS_GROUP_NAME ,
		usg.USERS_GROUP_NAME  || ',' ||ugl.LEVELNAME "LATEST_STATUS",
		mpux.USERS_NAME "APPROVTER_NAME",
		mpjt.approve_type,
		mpjc.alert_approve,
		mpjc.alert_close,
		mah.APPROVAL_STATUS
	FROM
		mp_request_job mprj
	LEFT JOIN mp_users mpu ON
		mpu.users_id = mprj.users_id
	LEFT JOIN mp_department mpdpt ON
		mpdpt.department_id = mprj.department_id
	LEFT JOIN mp_division mpdi ON
		mpdi.division_id = mprj.division_id
	LEFT JOIN mp_job_position mpjp ON
		mpjp.job_position_id = mprj.job_position_id
	LEFT JOIN mp_job_type mpjt ON
		mpjt.job_type_id = mpjp.job_type_id
	LEFT JOIN mp_job_condition mpjc ON
		mpjc.job_type_id = mpjt.job_type_id
	LEFT JOIN MP_APPROVAL_HIST mah ON mah.approval_hist_id  = mprj.latest_approved_hist_id
	LEFT JOIN mp_users_group_level ugl ON ugl.LEVELNO  = mah.LEVELNO
	LEFT JOIN mp_users mpux ON mpux.users_id  = mah.USERS_ID
	LEFT JOIN mp_users_group usg ON usg.USERS_GROUP_ID  = mah.USERS_GROUP_ID
	
	WHERE
		1 = 1
		AND mprj.flow_status <> 'NEW'
		${childSql}
		
        )
	WHERE 1=1 ${subsql}
ORDER BY
	request_job_id

	`
	console.log(sql)

	const result = await conn.execute(sql)

	// const jobRequestIdSet = result.rows.map()
	// jobRequestResult.map((item) => {
	// 	item
	// })

	// res.resly(200, { data: sql })
	res.resly(200, { data: result.rows })
}

//
