const services = require("../../services")
const lib = require("../../lib")
const bcrypt = require("bcrypt")
const Errorly = require("../../lib/response/Errorly")

module.exports = async (req, res) => {
	const conn = await req.conn
	const body = req.body
	console.log(body)
	const users_id = req.decodedToken.USERS_ID

	let sql = `
    UPDATE mp_request_job
    SET 
    CLOSE_FLAG = 1,
    CLOSE_DATE = sysdate,
    CLOSE_REMARK = '${body.close_remark}',
    CLOSE_BY = ${users_id},
    FLOW_STATUS = 'CLOSE'
    WHERE request_job_id='${body.request_job_id}'
    `

	console.log(sql)
	const bindParams2 = {}

	const result = await conn.execute(sql, bindParams2)

	await conn.commit()

	res.resly(200, {})
}
