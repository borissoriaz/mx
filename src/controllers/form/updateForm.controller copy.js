const services = require("../../services")
const lib = require("../../lib")
const bcrypt = require("bcrypt")
const Errorly = require("../../lib/response/Errorly")

module.exports = async (req, res) => {
	const conn = await req.conn
	const body = req.body
	console.log(body)
	const users_id = req.decodedToken.USERS_ID
	const sql = `

    UPDATE mp_request_job
    SET 
        job_position_id = to_number(${body.job_position_id}),
        plantno = to_number(${body.plantno}),
        division_id = to_number(${body.division_id}),
        department_id = to_number(${body.department_id}),
        section_id = to_number(${body.section_id}),
        tel = '${body.tel}',
        request_amount = to_number(${body.request_amount}),
        request_date = TO_DATE('${body.request_date}', 'dd/mm/yyyy'),
        request_type = to_number(${body.request_type}),
        remark = '${body.remark}',
        education_type_id = to_number(${body.education_type_id}),
        education_other = NULL,
        gender=to_number(${body.gender}),
        age=to_number(${body.age}),
        shiftwork=to_number(${body.shiftwork}),
        work_exp=to_number(${body.work_exp}),
        computer_skill='${body.computer_skill}',
        talent='${body.talent}',
        updateby=to_number(${users_id}),
        updatedate=sysdate,
        man_act= to_number(${body.man_act}),
        dept_quota=to_number(${body.dept_quota}),
        excelflag=to_number(${body.excelflag}),
        pointflag=to_number(${body.pointflag})
    WHERE request_job_id = '${body.request_job_id}'

     `

	console.log(sql)
	const bindParams = {}

	const result = await conn.execute(sql, bindParams)

	if (req.body.job_replace.length > 0) {
		const replaceJobSql = `
        UPDATE mp_request_replace
        SET
        resign_type= :resign_type,
        resign_date=TO_DATE(:resign_date, 'dd/mm/yyyy'),
        emp_id=:emp_id,
        emp_position=:emp_position,
        emp_name=:emp_name,
        updateby=to_number(${users_id}),
        updatedate=sysdate
        WHERE request_replace_id = '${body.request_replace_id}'

   
    `

		const bindParams3 = req.body.job_replace

		console.log(replaceJobSql)
		console.log("bindParams3", bindParams3)

		const replaceJObResult = await conn.executeMany(replaceJobSql, bindParams3)
	}

	await conn.commit()

	res.resly(200, {})
}
