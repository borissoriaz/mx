module.exports = {
	get auth() {
		return require("./auth")
	},
	get master() {
		return require("./master")
	},
	get blaze() {
		return require("./blaze")
	},
	get form() {
		return require("./form")
	},
	get approval() {
		return require("./approval")
	},
	get hr() {
		return require("./hr")
	},
}
