module.exports = {
  get signal() {
    return require("./signal.controller")
  },
  get ora() {
    return require("./ora.controller")
  },
}
